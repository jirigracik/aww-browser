import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubredditBrowserComponent} from './subreddit-browser/subreddit-browser.component';
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [SubredditBrowserComponent],
  exports: [
    SubredditBrowserComponent
  ],
  imports: [
    SharedModule
  ]
})
export class RedditModule {
}
