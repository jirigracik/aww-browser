import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScrollReporterDirective} from "../scroll-reporter.directive";


@NgModule({
  declarations: [ScrollReporterDirective],
  imports: [CommonModule],
  exports: [ScrollReporterDirective, CommonModule]
})
export class SharedModule {
}
