import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RedditModule} from "./reddit/reddit.module";
import {HttpClientModule} from "@angular/common/http";
import { ScrollReporterDirective } from './scroll-reporter.directive';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RedditModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
