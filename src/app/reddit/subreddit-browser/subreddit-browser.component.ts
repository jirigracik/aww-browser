import {Component, OnInit} from '@angular/core';
import {RedditService} from "../reddit.service";

@Component({
  selector: 'app-subreddit-browser',
  templateUrl: './subreddit-browser.component.html',
  styleUrls: ['./subreddit-browser.component.scss']
})
export class SubredditBrowserComponent implements OnInit {

  public posts: Post[] = [];
  private lastElementRectangle = new DOMRect();
  private obtainingPosts: boolean = false;

  constructor(private redditService: RedditService) {
    this.addPosts();
  }

  private filterPosts(posts: Post[]) {
    return posts.filter(p => p.url.endsWith('jpg'));
  }

  ngOnInit() {
  }

  private addPosts() {
    const lastPost = this.posts.length > 0 ? this.posts[this.posts.length - 1] : {id: undefined};
    this.obtainingPosts = true;
    this.redditService
      .getPosts('aww', lastPost.id)
      .subscribe(p => {
        this.posts.push(...this.filterPosts(p));
        this.obtainingPosts = false;
      });
  }

  onScroll(lastElementRectangle: DOMRect): void {
    this.lastElementRectangle = lastElementRectangle;
    this.addPostsIfNecessary();
  }

  private addPostsIfNecessary() {
    const bodyHeight = window.innerHeight;
    if (bodyHeight >= (this.lastElementRectangle.top - this.lastElementRectangle.height) && !this.obtainingPosts) {
      this.addPosts();
    }
  }
}
