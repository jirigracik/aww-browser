import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubredditBrowserComponent } from './subreddit-browser.component';

describe('SubredditBrowserComponent', () => {
  let component: SubredditBrowserComponent;
  let fixture: ComponentFixture<SubredditBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubredditBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubredditBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
