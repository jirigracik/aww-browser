class Post{
  id: string;
  permalink: string;
  url: string;
  thumbnail: string;
  title: string;
}
