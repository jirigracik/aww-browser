import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

type Item<T> = { data: T };

@Injectable({
  providedIn: 'root'
})
export class RedditService {

  constructor(private http: HttpClient) {
  }

  getPosts(subreddit: string, after?: string): Observable<Post[]> {
    return this.http.get<RedditResponse<Item<Post>>>(`http://www.reddit.com/r/${subreddit}/top.json?limit=1000${after?'&after=t3_' + after : ''}`)
      .pipe(
        map(response => response.data.children.map(item => item.data))
      )
  }
}
