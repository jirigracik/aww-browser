import {Directive, ElementRef, EventEmitter, HostListener, OnInit, Output} from '@angular/core';

@Directive({
  selector: '[scrollReporter]'
})
export class ScrollReporterDirective implements OnInit {

  @Output() onScroll = new EventEmitter<DOMRect>();

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.scroll();
  }

  @HostListener('window:scroll', ['$event'])
  scroll() {
    this.onScroll.emit(this.el.nativeElement.getBoundingClientRect());
  }

}
